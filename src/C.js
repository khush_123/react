import React, {Component} from 'react';

 class C extends Component{
   render() {
        
        return (
            <div>
                The data from class B is:{this.props.dataFromB}
            </div>
        );
    }
}

export default C;