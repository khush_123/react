import React, {Component} from 'react';
import B from './B.js';
import C from './C.js';

class Parent extends Component {   
render() {
     
        return (
            <div>
              <B />
              <C />  
            </div>
        );
    }
}

export default Parent;